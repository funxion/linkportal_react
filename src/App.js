/* eslint-disable no-console */
import React, { Component } from 'react';
// import shortid from 'shortid';
import firebase from 'firebase';
import './App.css';
import TabRow from './components/TabRow';
// import testTabs from './modules/testTabData';
import UserBar from './components/UserBar';
import Toolbar from './components/Toolbar';
import fire from './fire';
import IntroJumbotron from './IntroJumbotron';
import WaveAnimationCanvas from './WaveAnimationCanvas';
import LoadingSpinner from './LoadingSpinner';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      tabs: {},
      currentTabId: '',
      loggedIn: false,
      loading: true,
      userData: {},
    };
  }

  componentWillMount = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        const userData = {
          displayName: user.displayName,
          email: user.email,
          photoUrl: user.photoUrl,
          uid: user.uid,
        };

        this.setState({ loggedIn: true, userData });
        const userRef = fire.database().ref(`tabs/${user.uid}`);
        userRef.once('value').then((snapshot) => {
          const value = snapshot.val();

          // Create initial tab if user data is not found
          if (!value) {
            this.handleAddTab('Default')
              .then(() => {
                console.log(`Initial data for user ${user.displayName} created`);
                this.setState({ currentTabId: this.getFirstTabId(), loading: false });
              })
              .catch(err => console.log('Error creating initial data', err));
          } else {
            this.setState({ currentTabId: this.getFirstTabId(), loading: false });
          }

          // Set listeners to each tab's links
          Object.values(this.state.tabs).forEach((tab) => {
            const tabRef = fire.database().ref(`tabs/${user.uid}/${tab.id}/links`);
            tabRef.on('child_added', (snap) => {
              const { tabs } = this.state;
              const link = snap.val();
              if (link) {
                tabs[tabRef.parent.key].links[link.id] = link;
                this.setState({ tabs });
              }
            });
          });
        });

        // Tab added
        userRef.on('child_added', (snap) => {
          const tab = snap.val();
          if (tab) {
            const { tabs } = this.state;
            tabs[tab.id] = tab;
            this.setState({ tabs });
          }
        });
      } else {
        this.setState({ loading: false, tabs: { 123: { id: 123, title: 'placeholder' }, currentTabId: 123 } });
      }
    });
  }

  onSelectTab = (id) => {
    this.setState({ currentTabId: id });
  }

  getFirstTabId = () => {
    let id = null;

    if (this.state.tabs && Object.keys(this.state.tabs).length) {
      const keys = Object.keys(this.state.tabs);
      [id] = keys.sort((a, b) => this.state.tabs[a].index - this.state.tabs[b].index);
    }

    return id;
  }

  setLoginState = (val) => {
    this.setState({ loggedIn: val });
  }

  setLoadingState = (val) => {
    this.setState({ loading: val });
  }

  handleAddLink = (link) => {
    const linksRef = fire.database().ref(`tabs/${this.state.userData.uid}/${this.state.currentTabId}/links`).push();
    const item = { ...link, id: linksRef.getKey() };

    if (!item.url.startsWith('http')) {
      item.url = `http://${item.url}`;
    }

    return linksRef.set(item)
      .then(() => {
        console.log('Link added');
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleEditLink = (link) => {
    const updatedLink = { ...link };
    const { tabs } = this.state;
    const currentTab = tabs[this.state.currentTabId];
    const linkRef = fire.database().ref(`tabs/${this.state.userData.uid}/${currentTab.id}/links/${link.id}`);

    if (!updatedLink.url.startsWith('http')) {
      updatedLink.url = `http://${updatedLink.url}`;
    }

    currentTab.links[link.id] = link;
    this.setState({ tabs });
    return linkRef.update({ ...link })
      .then(() => {
        // Update OK
      })
      .catch(err => console.log(err));
  };

  handleRemoveLink = (link) => {
    const confirm = window.confirm(`Delete ${link.title}?`); // eslint-disable-line
    if (confirm) {
      const linksRef = fire.database().ref(`tabs/${this.state.userData.uid}/${this.state.currentTabId}/links/${link.id}`);
      linksRef.remove()
        .then(() => {
          const { tabs } = this.state;
          delete tabs[this.state.currentTabId].links[link.id];
          this.setState({ tabs });
        })
        .catch(err => console.log(err));
    }
  };

  handleAddTab = (title) => {
    const tabsRef = fire.database().ref(`tabs/${this.state.userData.uid}`).push();
    const item = {
      id: tabsRef.getKey(),
      title,
      links: {},
    };
    return tabsRef.set(item)
      .then(() => {
        const { tabs } = this.state;
        tabs[item.id] = item;
        const linkRef = fire.database().ref(`tabs/${this.state.userData.uid}/${item.id}/links`);
        linkRef.on('child_added', (snap) => {
          const link = snap.val();
          tabs[item.id].links[link.id] = link;
          this.setState({ tabs });
        });
      })
      .catch(err => console.log(err));
  }

  handleEditTab = (e) => {
    const { tabs } = this.state;
    const currentTab = tabs[this.state.currentTabId];
    const tabRef = fire.database().ref(`tabs/${this.state.userData.uid}/${currentTab.id}`);
    currentTab.title = e.target.value;
    this.setState({ tabs });
    tabRef.update({ title: e.target.value })
      .then(() => {
        // Update OK
      })
      .catch(err => console.log(err));
  }

  handleRemoveTab = () => {
    const { tabs } = this.state;
    const currentTab = tabs[this.state.currentTabId];
    const tabRef = fire.database().ref(`tabs/${this.state.userData.uid}/${currentTab.id}`);

    return tabRef.remove()
      .then(() => {
        delete tabs[this.state.currentTabId];
        tabRef.off('child_added');

        /*
        console.log(currentTab, currentIndex, currentTabId, tabs);
        if (currentIndex >= Object.keys(tabs).length) {
          currentTabId = Object.values(tabs).sort((a, b) => b.index - a.index)[0].id;
        }
        */
        this.setState({ currentTabId: this.getFirstTabId(), tabs });
      })
      .catch(err => console.log(err));
  }

  render() {
    const content =
      this.state.loggedIn && !this.state.loading
        ? (
          <div style={{ textAlign: 'center' }}>
            <Toolbar
              handleAddTab={this.handleAddTab}
              handleEditTab={this.handleEditTab}
              handleRemoveTab={this.handleRemoveTab}
              handleAddLink={this.handleAddLink}
              tab={this.state.tabs[this.state.currentTabId]}
              tabs={this.state.tabs}
            />
            <TabRow
              tabs={this.state.tabs}
              currentTabId={this.state.currentTabId}
              onSelectTab={this.onSelectTab}
              handleRemoveLink={this.handleRemoveLink}
              handleEditLink={this.handleEditLink}
            />
          </div>
        )
        : (
          <div
            style={{
              height: '70vh',
              margin: '0 -15px',
            }}
          >
            <WaveAnimationCanvas />
            <IntroJumbotron />
          </div>
        );

    return (
      <div className="App container-fluid">
        <UserBar
          loggedIn={this.state.loggedIn}
          setLoginState={this.setLoginState}
          openModal={this.openModal}
          loading={this.state.loading}
          userData={this.state.userData}
          setLoadingState={this.setLoadingState}
        />

        {
          this.state.loading
          ? <LoadingSpinner />
          : content
        }
      </div>
    );
  }
}
