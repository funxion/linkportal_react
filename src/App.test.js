/* eslint-env jest */
import React from 'react';
import { shallow } from 'enzyme';
import ToJson from 'enzyme-to-json';
import App from './App';
// import AddTabModal from './components/modals/AddTabModal';
// import RemoveTabModal from './components/modals/RemoveTabModal';
// import EditTabModal from './components/modals/EditTabModal';

it('adds a valid tab', () => {
  const wrapper = shallow(<App />);
  wrapper.instance().addTab('awesomeTab');
  const lastIdx = wrapper.state('tabs').length - 1;
  expect(wrapper.state('tabs')[lastIdx]).toEqual({
    id: expect.any(String),
    title: 'awesomeTab',
    links: [],
  });
});

it('changes the correct tab title', () => {
  const wrapper = shallow(<App />);
  wrapper.instance().addTab('boring tab name');
  const lastIdx = wrapper.state('tabs').length - 1;
  wrapper.instance().onSelectTab(lastIdx);
  wrapper.instance().handleEditTab({ target: { value: 'awesome tab name!' } });
  expect(wrapper.state('tabs')[lastIdx].title).toBe('awesome tab name!');
});

it('removes last tab and sets tab index to tabs length - 1', () => {
  const wrapper = shallow(<App />);
  wrapper.instance().addTab('boring tab name');
  const initialLength = wrapper.state('tabs').length;
  const lastIdx = wrapper.state('tabs').length - 1;
  wrapper.instance().onSelectTab(lastIdx);
  wrapper.instance().handleRemoveTab();
  expect(wrapper.state('tabs')).toHaveLength(initialLength - 1);
  expect(wrapper.state('currentTabIndex')).toBe(lastIdx - 1);
});
