import React from 'react';
import { Jumbotron, PageHeader, Col } from 'react-bootstrap';

const IntroJumbotron = () => (
  <Col
    sm={12}
    md={6}
    mdOffset={3}
    style={{ position: 'absolute', top: '20%' }}
  >
    <Jumbotron style={{ background: 'rgba(250, 250, 250, .95)', border: '1px solid #ddd' }}>
      <PageHeader>
        Welcome to Linky<br />
        <small>Nice and simple bookmark page</small>
      </PageHeader>
      <p>
        Log in to start using Linky. Your bookmarks will be
        synced with the cloud so you can access them anywhere!
      </p>
    </Jumbotron>
  </Col>
);

export default IntroJumbotron;
