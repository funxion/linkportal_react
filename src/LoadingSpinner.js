import React from 'react';

const LoadingSpinner = () => {
  const styles = {
    position: 'absolute',
    width: '40vh',
    height: '40vh',
    left: '50%',
    top: '50%',
    border: '24px double #eff',
    borderRight: 'none',
    borderLeft: 'none',
    margin: '0 auto',
    borderRadius: '50%',
    animation: 'spin 2s linear infinite',
  };

  return <div className="spinner" style={styles} />;
};

export default LoadingSpinner;
