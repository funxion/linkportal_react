/* eslint-disable object-curly-newline */
import React, { PureComponent } from 'react';

let loop;

class WaveAnimationCanvas extends PureComponent {
  constructor() {
    super();
    this.width = null;
    this.height = null;
    this.canvasStyle = null;
  }

  componentDidMount() {
    const canvas = document.getElementById('WaveCanvas');
    const ctx = canvas.getContext('2d');
    const pos = { x: -50, y: 0 };
    const halfHeight = this.height / 2;
    ctx.translate(0, halfHeight);

    let counter = 0;
    let cycle = 0;
    const cycles = [
      { hue: 30, startY: 0, lineWidth: 50, amp: 0.4, offset: 0 },
      { hue: 50, startY: 0, lineWidth: 75, amp: 0.2, offset: 1.5 },
      { hue: 230, startY: 0, lineWidth: 40, amp: 0.6, offset: 0.5 },
      { hue: 0, startY: 0, lineWidth: 50, amp: 0.3, offset: 0.7 },
      { hue: 150, startY: 0, lineWidth: 60, amp: 0.8, offset: 2.0 },
    ];

    function initCycle() {
      ctx.lineWidth = cycles[cycle].lineWidth;
      ctx.strokeStyle = `hsla(${cycles[cycle].hue}, 100%, 80%, .03)`;
      counter = cycles[cycle].offset;
    }

    initCycle();

    (loop = () => {
      if (cycle < cycles.length) {
        // console.log('anim');
        window.requestAnimationFrame(loop);
        counter += 0.01;
        const initialY = (cycles[cycle].startY * halfHeight);
        const finalAmp = cycles[cycle].amp * halfHeight;
        const mod1 = Math.sin(counter * 10);
        pos.y = initialY + (mod1 * finalAmp);
        ctx.lineTo(pos.x, pos.y);
        ctx.stroke();
        pos.x += 20;
        if (pos.x > this.width + 500) {
          cycle += 1;
          if (cycle >= cycles.length) return;
          initCycle();
          pos.x = -50;
          ctx.beginPath();
        }
      }
    })();
  }

  render() {
    this.width = window.innerWidth;
    this.height = window.innerHeight / 2;

    this.canvasStyle = {
      position: 'absolute',
      display: 'block',
      left: 0,
      top: this.height / 2,
      width: '100%',
      height: this.height,
    };
    return <canvas width={this.width} height={this.height} style={this.canvasStyle} id="WaveCanvas" />;
  }
}

export default WaveAnimationCanvas;
