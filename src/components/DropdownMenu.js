import React from 'react';
import PropTypes from 'prop-types';
import { NavDropdown, MenuItem, Glyphicon, Tooltip, OverlayTrigger } from 'react-bootstrap';

const DropdownMenu = ({
  openModal, tabs,
}) => {
  const onOpenModal = stateName => () => {
    openModal(stateName);
  };

  let removeMenuItem;
  const tooltip = <Tooltip id="tabsLengthTooltip">You cannot remove last remaining tab</Tooltip>;

  if (tabs.length > 1) {
    removeMenuItem = <MenuItem onClick={onOpenModal('removeTab')}><Glyphicon glyph="minus" /> Remove tab</MenuItem>;
  } else {
    removeMenuItem = (
      <OverlayTrigger placement="bottom" overlay={tooltip}>
        <MenuItem disabled><Glyphicon glyph="minus" /> Remove tab</MenuItem>
      </OverlayTrigger>
    );
  }

  return (
    <NavDropdown title="MENU" id="dropdown-menu" noCaret>
      <MenuItem onClick={onOpenModal('addTab')}><Glyphicon glyph="plus" /> Add tab</MenuItem>
      {removeMenuItem}
      <MenuItem onClick={onOpenModal('editTab')}><Glyphicon glyph="edit" /> Edit tab</MenuItem>
      <MenuItem divider />
      <MenuItem><Glyphicon glyph="import" /> Import</MenuItem>
      <MenuItem><Glyphicon glyph="export" /> Export</MenuItem>
    </NavDropdown>
  );
};

DropdownMenu.propTypes = {
  openModal: PropTypes.func.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default DropdownMenu;
