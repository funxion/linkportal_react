import React from 'react';
import PropTypes from 'prop-types';
import { SplitButton, MenuItem } from 'react-bootstrap';
import EditLinkModal from './modals/EditLinkModal';

export default class Link extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editModal: false,
    };
  }

  openEditLinkModal = () => this.setState({ editModal: true });

  closeEditLinkModal = () => this.setState({ editModal: false });

  removeLink = () => this.props.handleRemoveLink(this.props.link);

  navigate = (e) => {
    e.preventDefault();
    if (typeof this.props.link.clicks !== 'undefined') {
      this.props.link.clicks += 1;
    } else {
      this.props.link.clicks = 1;
    }

    this.props.handleEditLink(this.props.link)
      .then(() => {
        window.location.href = this.props.link.url.startsWith('http') ? this.props.link.url : `http://${this.props.link.url}`;
      })
      .catch(() => {
        window.location.href = this.props.link.url.startsWith('http') ? this.props.link.url : `http://${this.props.link.url}`;
      });
  }

  render = () => (
    <SplitButton
      title={this.props.link.title}
      key={this.props.link.id}
      id={`button-${this.props.link.title}`}
      bsSize="lg"
      href={this.props.link.url.startsWith('http') ? this.props.link.url : `http://${this.props.link.url}`}
      toggleLabel={this.props.link.description || ''}
    >
      <MenuItem onClick={this.openEditLinkModal}>Edit</MenuItem>
      <MenuItem onClick={this.removeLink}>Remove</MenuItem>
      <EditLinkModal
        closeModal={this.closeEditLinkModal}
        showModal={this.state.editModal}
        link={this.props.link}
        handleEditLink={this.props.handleEditLink}
      />
    </SplitButton>
  );
}


Link.propTypes = {
  link: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    description: PropTypes.string,
    clicks: PropTypes.number,
  }).isRequired,
  handleRemoveLink: PropTypes.func.isRequired,
  handleEditLink: PropTypes.func.isRequired,
};
