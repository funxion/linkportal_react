import React from 'react';
import PropTypes from 'prop-types';
import { ButtonToolbar } from 'react-bootstrap';
import Link from './Link';

const TabContent = ({ links, handleRemoveLink, handleEditLink }) =>
  (
    <ButtonToolbar>
      {
        links && Object.keys(links).map(linkId => (
          <Link
            link={links[linkId]}
            key={linkId}
            handleRemoveLink={handleRemoveLink}
            handleEditLink={handleEditLink}
          />
        ))
      }
    </ButtonToolbar>
  );


TabContent.propTypes = {
  links: PropTypes.objectOf(PropTypes.object),
  handleEditLink: PropTypes.func.isRequired,
  handleRemoveLink: PropTypes.func.isRequired,
};

TabContent.defaultProps = { links: {} };

export default TabContent;
