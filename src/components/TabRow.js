import React from 'react';
import PropTypes from 'prop-types';
import { Tab, Row, Col, Nav, NavItem } from 'react-bootstrap';
import TabContent from './TabContent';
// import DropdownMenu from './DropdownMenu';

const TabRow = ({
  onSelectTab, currentTabId, tabs, handleEditLink, handleRemoveLink,
}) => {
  const renderTab = id => (
    <NavItem eventKey={id} key={id}>
      {tabs[id].title}
    </NavItem>
  );

  const renderContent = id => (
    <Tab.Pane eventKey={id} key={id}>
      <TabContent
        links={tabs[id].links}
        handleRemoveLink={handleRemoveLink}
        handleEditLink={handleEditLink}
      />
    </Tab.Pane>
  );

  const contentStyles = {
    border: '1px solid',
    borderColor: 'var(--main-border-color)',
    borderTop: 'none',
    borderRadius: '0 5px 5px 5px',
    padding: 20,
  };

  return (
    <Tab.Container id="tabs" activeKey={currentTabId} onSelect={onSelectTab} style={{ marginTop: 10 }}>
      <Row className="clearfix">
        <Col sm={12}>
          <Nav bsStyle="tabs" style={{ marginRight: 3, marginBottom: -1 }}>
            {/* <DropdownMenu openModal={openModal} tabs={tabs} /> */}
            {Object.keys(tabs).map(renderTab)}
          </Nav>
        </Col>
        <Col sm={12}>
          <Tab.Content animation style={contentStyles}>
            {Object.keys(tabs).map(renderContent)}
          </Tab.Content>
        </Col>
      </Row>
    </Tab.Container>
  );
};

TabRow.propTypes = {
  onSelectTab: PropTypes.func.isRequired,
  currentTabId: PropTypes.string.isRequired,
  tabs: PropTypes.objectOf(PropTypes.object).isRequired,
  handleEditLink: PropTypes.func.isRequired,
  handleRemoveLink: PropTypes.func.isRequired,
};

export default TabRow;
