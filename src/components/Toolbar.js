import React, { Component } from 'react';
import { Button, Glyphicon } from 'react-bootstrap';
import PropTypes from 'prop-types';
import AddTabModal from './modals/AddTabModal';
import RemoveTabModal from './modals/RemoveTabModal';
import EditTabModal from './modals/EditTabModal';
import AddLinkModal from './modals/AddLinkModal';

class Toolbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      add: false,
      edit: false,
      remove: false,
      addLink: false,
    };
  }

  openModal = (modalName) => {
    const obj = this.state;
    if (obj.hasOwnProperty(modalName)) {
      obj[modalName] = true;
      this.setState(obj);
    } else {
      throw new Error(`${modalName} is not a valid modal state`);
    }
  }

  closeModal = (modalName) => {
    const obj = this.state;
    if (obj.hasOwnProperty(modalName)) {
      obj[modalName] = false;
      this.setState(obj);
    } else {
      throw new Error(`${modalName} is not a valid modal state`);
    }
  }

  render() {
    const styles = {
      border: '1px solid #ddd',
      borderTop: 'none',
      borderRadius: '0 0 5px 5px',
      background: 'white',
      display: 'inline-block',
      boxShadow: '0 3px 3px rgba(0, 0, 0, .2)',
      borderColor: 'var(--main-border-color)',
      animation: 'scaleToolbar 1s normal forwards',
    };
    return this.props.tab && (
      <div>
        <div style={styles}>
          <Button onClick={() => this.openModal('addLink')} bsStyle="link" className="top-menu-button"><Glyphicon glyph="plus" /> Link</Button>
        </div>
        <div style={styles}>
          <Button onClick={() => this.openModal('add')} bsStyle="link" className="top-menu-button"><Glyphicon glyph="plus" /> Add tab</Button>
          <Button onClick={() => this.openModal('edit')} bsStyle="link" className="top-menu-button"><Glyphicon glyph="edit" /> Edit tab</Button>
          {
            Object.keys(this.props.tabs).length > 1
            ? <Button onClick={() => this.openModal('remove')} bsStyle="link" className="top-menu-button"><Glyphicon glyph="minus" /> Remove tab</Button>
            : <Button disabled bsStyle="link" className="top-menu-button"><Glyphicon glyph="minus" /> Cannot remove only tab</Button>
          }
        </div>
        {
          /*
          <div style={styles}>
            <Button
              disabled
              bsStyle="link"
              className="top-menu-button"
            >
              <Glyphicon glyph="import" /> Import
            </Button>
            <Button
              disabled
              bsStyle="link"
              className="top-menu-button"
            >
              <Glyphicon glyph="export" /> Export
            </Button>
          </div>
          */
        }

        <AddTabModal
          closeModal={this.closeModal}
          showModal={this.state.add}
          handleAddTab={this.props.handleAddTab}
        />

        <EditTabModal
          closeModal={this.closeModal}
          showModal={this.state.edit}
          tab={this.props.tab}
          handleEditTab={this.props.handleEditTab}
        />

        <RemoveTabModal
          closeModal={this.closeModal}
          showModal={this.state.remove}
          tab={this.props.tab}
          handleRemoveTab={this.props.handleRemoveTab}
        />

        <AddLinkModal
          closeModal={this.closeModal}
          showModal={this.state.addLink}
          handleAddLink={this.props.handleAddLink}
        />
      </div>
    );
  }
}

Toolbar.propTypes = {
  handleAddTab: PropTypes.func.isRequired,
  handleAddLink: PropTypes.func.isRequired,
  handleEditTab: PropTypes.func.isRequired,
  handleRemoveTab: PropTypes.func.isRequired,
  tabs: PropTypes.shape({}).isRequired,
  tab: PropTypes.shape({
    title: PropTypes.string.isRequired,
    links: PropTypes.object,
  }).isRequired,
};

export default Toolbar;
