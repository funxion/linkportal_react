import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import firebase from 'firebase';
import LoginModal from './modals/LoginModal';

class UserBar extends Component {
  constructor(props) {
    super(props);
    this.state = { showLoginModal: false };
  }

  openLoginModal = () => {
    this.setState({ showLoginModal: true });
  }

  closeLoginModal = () => {
    this.setState({ showLoginModal: false });
  }

  logout = () => {
    this.props.setLoadingState(true);
    firebase.auth().signOut()
      .then((res) => {
        this.props.setLoginState(false);
        this.props.setLoadingState(false);
      })
      .catch((err) => {
        console.log(err);
        this.props.setLoadingState(false);
      });
  }

  rowStyle = {
    margin: '0 -15px',
    borderBottom: '1px solid #ddd',
    borderColor: 'var(--main-border-color)',
    padding: '5px',
    background: 'white',
  };

  render = () => (
    <Row style={this.rowStyle}>
      <Col sm={12} className="toolbar">
        <LoginModal
          show={this.state.showLoginModal}
          closeModal={this.closeLoginModal}
        />
        <div style={{ flex: '0 1 auto', fontSize: 24 }}>
          LINKY
        </div>
        <div style={{ flex: '4 1 auto', textAlign: 'right' }}>
          {
            this.props.loggedIn && this.props.loading === false
            ? <div>Logged in as <b>{this.props.userData.displayName}</b></div>
            : null
          }
        </div>
        <div style={{ flex: '0 4 auto', textAlign: 'center' }}>
          <div>
            {
              this.props.loggedIn
              ? this.props.loading === false && <Button bsStyle="link" onClick={this.logout}>Logout</Button>
              : this.props.loading === false && <Button bsStyle="link" onClick={this.openLoginModal}>Login/sign up</Button>
              }
          </div>
        </div>
      </Col>
    </Row>
  );
}

UserBar.propTypes = {
  loggedIn: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  setLoginState: PropTypes.func.isRequired,
  setLoadingState: PropTypes.func.isRequired,
  userData: PropTypes.shape({}).isRequired,
};

export default UserBar;
