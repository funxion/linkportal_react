import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

class AddLinkModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      newLink: {
        title: '',
        description: '',
        url: '',
        clicks: 0,
      },
    };
  }

  onExited = () => {
    this.reset();
  };

  onCloseModal = () => {
    this.props.closeModal('addLink');
  };

  reset = () => {
    this.setState({
      newLink: {
        title: '',
        url: '',
      },
    });
  };

  validateTitle = () => {
    const titleLength = this.state.newLink.title.length;
    if (titleLength > 0) return 'success';
    return 'error';
  };

  validateUrl = () => {
    const urlLength = this.state.newLink.url.length;
    if (urlLength > 0) return 'success';
    return 'error';
  };

  handleChange = (e) => {
    const newLink = { ...this.state.newLink };
    newLink[e.target.name] = e.target.value;
    this.setState({ newLink });
  };

  insertNewLink = (e) => {
    e.preventDefault();
    this.props.handleAddLink(this.state.newLink)
      .then(() => {
        this.props.closeModal('addLink');
      });
  };

  render() {
    return (
      <Modal
        bsSize="small"
        show={this.props.showModal}
        onHide={this.onCloseModal}
        onExited={this.onExited}
      >
        <Modal.Header closeButton>
          <Modal.Title>Add link</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <FormGroup controlId="linkUrlInput" validationState={this.validateUrl()}>
              <ControlLabel>Link URL</ControlLabel>
              <FormControl
                type="url"
                name="url"
                value={this.state.newLink.url}
                placeholder="Link URL"
                onChange={this.handleChange}
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup controlId="linkTitleInput" validationState={this.validateTitle()}>
              <ControlLabel>Link title</ControlLabel>
              <FormControl
                type="text"
                name="title"
                value={this.state.newLink.title}
                placeholder="Link title"
                onChange={this.handleChange}
                autoFocus
              />
              <FormControl.Feedback />
            </FormGroup>
            <FormGroup controlId="linkDescriptionInput">
              <ControlLabel>Description</ControlLabel>
              <FormControl
                type="textarea"
                name="description"
                value={this.state.newLink.description}
                placeholder="Description"
                onChange={this.handleChange}
              />
            </FormGroup>
            <div className="clearfix" style={{ borderTop: '1px solid #e5e5e5', paddingTop: 15 }}>
              <Button bsStyle="primary" className="pull-left" type="submit" onClick={this.insertNewLink}>Add</Button>
              <Button bsStyle="default" className="pull-right" onClick={this.onCloseModal}>Close</Button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    );
  }
}

AddLinkModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  handleAddLink: PropTypes.func.isRequired,
};

export default AddLinkModal;
