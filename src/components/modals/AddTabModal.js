import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

class AddTabModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      newTabTitle: '',
    };
  }

  onExited = () => {
    this.setState({ newTabTitle: '' });
  };

  onCloseModal = () => {
    this.props.closeModal('add');
  };

  getValidationState = () => {
    const titleLength = this.state.newTabTitle.length;
    if (titleLength > 0) return 'success';
    return 'error';
  };

  handleChange = (e) => {
    this.setState({ newTabTitle: e.target.value });
  };

  insertNewTab = (e) => {
    e.preventDefault();
    this.props.handleAddTab(this.state.newTabTitle);
    this.props.closeModal('add');
  };

  render() {
    return (
      <Modal
        bsSize="small"
        show={this.props.showModal}
        onHide={this.onCloseModal}
        onExited={this.onExited}
      >
        <Modal.Header closeButton>
          <Modal.Title>Add tab</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form>
            <FormGroup controlId="tabTitleInput" validationState={this.getValidationState()}>
              <ControlLabel>Tab title</ControlLabel>
              <FormControl
                type="text"
                value={this.state.newTabTitle}
                placeholder="Enter tab title"
                onChange={this.handleChange}
                autoFocus
              />
              <FormControl.Feedback />
            </FormGroup>
            <div className="clearfix" style={{ borderTop: '1px solid #e5e5e5', paddingTop: 15 }}>
              <Button bsStyle="primary" className="pull-left" type="submit" onClick={this.insertNewTab}>Add</Button>
              <Button bsStyle="default" className="pull-right" onClick={this.onCloseModal}>Close</Button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    );
  }
}

AddTabModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  handleAddTab: PropTypes.func.isRequired,
};

export default AddTabModal;
