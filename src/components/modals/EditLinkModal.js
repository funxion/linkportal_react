import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

class EditLinkModal extends React.PureComponent {
  onCloseModal = () => {
    this.props.closeModal('editLink');
  };

  validateTitle = () => {
    const titleLength = this.props.link.title.length;
    if (titleLength > 0) return 'success';
    return 'error';
  };

  validateUrl = () => {
    const urlLength = this.props.link.url.length;
    if (urlLength > 0) return 'success';
    return 'error';
  };

  handleChange = (e) => {
    this.props.link[e.target.name] = e.target.value;
    this.props.handleEditLink(this.props.link);
  };

  render = () => (
    <Modal
      bsSize="small"
      show={this.props.showModal}
      onHide={this.onCloseModal}
      onExited={this.onExited}
    >
      <Modal.Header closeButton>
        <Modal.Title>Add link</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          <FormGroup controlId="linkUrlInput" validationState={this.validateUrl()}>
            <ControlLabel>Link URL</ControlLabel>
            <FormControl
              type="url"
              value={this.props.link.url}
              name="url"
              placeholder="Link URL"
              onChange={this.handleChange}
            />
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup controlId="linkTitleInput" validationState={this.validateTitle()}>
            <ControlLabel>Link title</ControlLabel>
            <FormControl
              type="text"
              value={this.props.link.title}
              placeholder="Link title"
              name="title"
              onChange={this.handleChange}
              autoFocus
            />
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup controlId="linkDescriptionInput">
            <ControlLabel>Description</ControlLabel>
            <FormControl
              type="textarea"
              value={this.props.link.description}
              name="description"
              placeholder="Description"
              onChange={this.handleChange}
            />
          </FormGroup>
          <div className="clearfix" style={{ borderTop: '1px solid #e5e5e5', paddingTop: 15 }}>
            <Button bsStyle="default" className="pull-right" onClick={this.onCloseModal}>Close</Button>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  );
}

EditLinkModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  handleEditLink: PropTypes.func.isRequired,
  link: PropTypes.shape({
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    description: PropTypes.string,
    clicks: PropTypes.number,
  }).isRequired,
};

export default EditLinkModal;
