import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

const EditTabModal = ({
  closeModal, handleEditTab, tab, showModal,
}) => {
  const getValidationState = () => {
    const titleLength = tab.title.length;
    if (titleLength > 0) return 'success';
    return 'error';
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    closeModal('edit');
  };

  const onCloseModal = () => {
    closeModal('edit');
  };

  return (
    <Modal
      bsSize="small"
      show={showModal}
      onHide={onCloseModal}
    >
      <Modal.Header closeButton>
        <Modal.Title>Edit tab {tab.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <FormGroup
            controlId="tabTitleInput"
            validationState={getValidationState()}
          >
            <ControlLabel>Tab title</ControlLabel>
            <FormControl
              type="text"
              value={tab.title}
              placeholder="Enter tab title"
              onChange={handleEditTab}
              autoFocus
            />
            <FormControl.Feedback />
          </FormGroup>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={onCloseModal}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
};

EditTabModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  handleEditTab: PropTypes.func.isRequired,
  tab: PropTypes.shape({
    title: PropTypes.string.isRequired,
    links: PropTypes.object,
  }).isRequired,
};

export default EditTabModal;
