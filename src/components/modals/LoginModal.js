import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import firebase from 'firebase';
import PropTypes from 'prop-types';

const LoginModal = ({ show, closeModal }) => {
  const signWithGoogle = () => {
    const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

    firebase.auth().signInWithRedirect(googleAuthProvider)
      .then(res => console.log('SUCCESS!', res))
      .catch(err => console.log('ERROR', err));
  };

  return (
    <Modal show={show} onHide={closeModal} bsSize="sm">
      <Modal.Header style={{ textAlign: 'center' }} closeButton>
        <h4>Select login method</h4>
      </Modal.Header>
      <Modal.Body>
        <Button block bsSize="lg" bsStyle="primary" onClick={signWithGoogle}>Google</Button>
        {/* <Button disabled block bsSize="lg" bsStyle="primary">Email and password</Button> */}
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={closeModal}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
};

LoginModal.propTypes = {
  show: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
};

export default LoginModal;
