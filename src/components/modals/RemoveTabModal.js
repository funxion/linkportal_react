import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'react-bootstrap';

const RemoveTabModal = ({
  showModal, closeModal, handleRemoveTab, tab,
}) => {
  const onCloseModal = () => {
    closeModal('remove');
  };

  const handleRemove = () => {
    handleRemoveTab()
      .then(() => {
        closeModal('remove');
      })
      .catch(err => console.log('Could not remove tab: ', err));
  };

  return (
    <Modal
      bsSize="small"
      show={showModal}
      onHide={onCloseModal}
    >
      <Modal.Header closeButton>
        <Modal.Title>Confirm removal of {tab.title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>Are you sure you want to remove the current tab? ( {tab.title} )</p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={handleRemove}>Yes</Button>
        <Button onClick={onCloseModal}>
          Cancel
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

RemoveTabModal.propTypes = {
  showModal: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  handleRemoveTab: PropTypes.func.isRequired,
  tab: PropTypes.shape({
    title: PropTypes.string.isRequired,
    links: PropTypes.object,
  }).isRequired,
};

export default RemoveTabModal;
