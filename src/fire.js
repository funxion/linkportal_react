import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyB1OcBuACD6N6c9gsUgesw1wXdfWFB4P50',
  authDomain: 'linky-app.firebaseapp.com',
  databaseURL: 'https://linky-app.firebaseio.com',
  projectId: 'linky-app',
  storageBucket: 'linky-app.appspot.com',
  messagingSenderId: '189314800429',
};

const fire = firebase.initializeApp(config);

export default fire;
