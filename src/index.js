/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
// TEMP FIX TO GET MODALS WORKING
import { Modal } from 'react-overlays';
Modal.prototype.componentWillMount = function () {
    this.focus = () => {};
};
// END FIX

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
